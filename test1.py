#Author:c1e4r

import sys
import getopt

#读取指定文件中的字符数
def read_str(file2):
    file = sys.argv[-1]
    file1 = open(file,'r+')
    count = 0
    for line in file1.readlines():
        count_line = len(line)
        count += count_line
    f = open(file2,'a+')
    f.write(file+",字符数:"+str(count)+'\n')
    file1.close()
    f.close()

#读取指定文件中的单词数
def read_word(file2):
    file = sys.argv[-1]
    file1 = open(file,'r+')
    count = 0
    for line in file1.readlines():
        line = line.replace(","," ")
        line = line.replace("."," ")
        line = line.replace("!"," ")
        line = line.replace("?"," ")
        line_word = len(line.split( ))
        count += line_word 
    f = open(file2,'a+')
    f.write(file+",单词数:"+str(count)+'\n')
    file1.close()
    f.close()

#读取指定文件中的行数
def read_line(file2):
    file = sys.argv[-1]
    file1 = open(file,'r+')
    count = 0
    for line in file1.readlines():
        count += 1
    f = open(file2,'a+')
    f.write(file+",行数:"+str(count)+'\n') 
    file1.close()
    f.close()
    
def main():
    file = "result.txt"
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hc:w:l:o:")
    except getopt.GetoptError:
        print("test.py [parameter] [input_file_name]")
        sys.exit(2)
    finally:
        pass
    for opt,arg in opts:
        if opt == '-h':
            print("test.py -c -w -l -o <outputfile> [input_file_name]") # -h 使用说明
            sys.exit()
        elif opt == "-c": 
            read_str(file)  # -c 返回文件的字符数
        elif opt == "-w":
            read_word(file) # -w 返回文件的单词总数
        elif opt == "-l":
            read_line(file) # -l 返回文件的总行数
        elif opt == "-o":
            file = arg      # -o 输出结果的文件   
  
if __name__ == "__main__":
    main()
    
    
